import React from "react";
import firstImageSmall from "./image1.png";
import firstImageLarge from "./image1_2x.png";
import secondImageSmall from "./image2.png";
import secondImageLarge from "./image2_2x.png";
import "./overview.scss";

const Overview = () => (
  <div className="overview container">
    <h1 className="lora-bold">Ukrainian Digital Volunteers</h1>
    <h2 className="lora-bold">IT послуги для хороших людей.</h2>
    <div className="flex">
      <div className="description">
        <span>
          Ми - команда спеціалістів в сфері IT послуг, які об'єдналися для того
          щоб допомогати українським проєктам ставати кращими.
        </span>
        <img src={secondImageSmall} alt="" srcSet={`${secondImageLarge} 2x`} />
      </div>
      <img
        className="background-image"
        src={firstImageSmall}
        alt=""
        srcSet={`${firstImageLarge} 2x`}
      />
    </div>
  </div>
);

export default Overview;
