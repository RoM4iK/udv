import React from "react";
import firstImageSmall from "./image1.png";
import firstImageLarge from "./image1_2x.png";
import secondImageSmall from "./image2.png";
import secondImageLarge from "./image2_2x.png";
import thirdImageSmall from "./image3.png";
import thirdImageLarge from "./image3_2x.png";
import "./services.scss";

const Services = () => (
  <div className="services">
    <div className="background-color"></div>
    <div className="container">
      <div className="title lora-bold">
        <span className="yellow">Наші</span> послуги
      </div>
      <div className="flex items">
        <div className="item">
          <img
            className="background-image"
            src={firstImageSmall}
            alt=""
            srcSet={`${firstImageLarge} 2x`}
          />
          <div>Дизайн та розробка сайтів, мобільних додатків</div>
        </div>
        <div className="item">
          <img
            className="background-image"
            src={secondImageSmall}
            alt=""
            srcSet={`${secondImageLarge} 2x`}
          />
          <div>Просуванная проекту в медіа-просторі</div>
        </div>
        <div className="item">
          <img
            className="background-image"
            src={thirdImageSmall}
            alt=""
            srcSet={`${thirdImageLarge} 2x`}
          />
          <div>Створенная та аналітика маркетингових стратегій</div>
        </div>
      </div>
    </div>
  </div>
);

export default Services;
