import React from "react";
import firstImageSmall from "./image1.png";
import firstImageLarge from "./image1_2x.png";
import secondImageSmall from "./image2.png";
import secondImageLarge from "./image2_2x.png";
import "./principles.scss";

const Principles = () => (
  <div className="principles">
    <div className="background-color"></div>
    <div className="container">
      <div className="title lora-bold">
        Принципи нашої <span className="yellow">роботи</span>
      </div>
      <div className="flex items">
        <div className="item">
          <img
            className="background-image"
            src={firstImageSmall}
            alt=""
            srcSet={`${firstImageLarge} 2x`}
          />
          <div>Ми надаємо наші послуги безкоштовно</div>
        </div>
        <div className="item">
          <img
            className="background-image"
            src={secondImageSmall}
            alt=""
            srcSet={`${secondImageLarge} 2x`}
          />
          <div>Викладаємо результати нашої роботи в open source</div>
        </div>
        <div className="item">
          <img
            className="background-image"
            src={secondImageSmall}
            alt=""
            srcSet={`${secondImageLarge} 2x`}
          />
          <div>Маємо прозору фінансову структуру</div>
        </div>
      </div>
    </div>
  </div>
);

export default Principles;
