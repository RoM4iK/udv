import React from "react";
import "./goal.scss";
import imageSmall from "./image.png";
import imageLarge from "./image_2x.png";

const Goal = () => (
  <div className="goal container flex">
    <img
      className="background-image"
      src={imageSmall}
      alt=""
      srcSet={`${imageLarge} 2x`}
    />

    <div>
      <div className="title lora-bold">
        <span className="yellow">Наша</span> мета
      </div>
      <div className="text">
        <span>
          Ми хочемо допомогати українським неприбутковим організаціям в їх
          розвитку.
        </span>
        <br />
        <span>
          В нашій країні дуже багато дійсно крутих організацій, і ми хочемо щоб
          про них почули усі.
        </span>
      </div>
    </div>
  </div>
);

export default Goal;
