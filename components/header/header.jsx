import React from "react";
import "./header.scss";

const Header = () => (
  <header className="header">
    <div className="container">
      <div className="content">
        <div className="logo">UDV</div>

        <nav className="navigation flex">
          <a href="#" className="item active">
            <span>Про нас</span>
          </a>
          <a href="#" className="item">
            <span>Послуги</span>
          </a>
          <a href="#" className="item">
            <span>Проекти</span>
          </a>
          <a href="#" className="item">
            <span>Співпраця</span>
          </a>
        </nav>
      </div>
    </div>
  </header>
);

export default Header;
