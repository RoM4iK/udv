import React from "react";
import ReactDOM from "react-dom";

import Header from "./components/header/header";
import Overview from "./components/overview/overview";
import Goal from "./components/goal/goal";
import Principles from "./components/principles/principles";
import Services from "./components/services/services";

const app = (
  <div className="app">
    <Header />
    <Overview />
    <Goal />
    <Principles />
    <Services />
  </div>
);

ReactDOM.render(app, document.querySelector(".main-container"));
